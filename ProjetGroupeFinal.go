package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/kare/base62"
	_ "github.com/lib/pq"
)

const (
	host     = "127.0.0.1"
	port     = 5432
	user     = "projet"
	password = "FX82jglN"
	dbname   = "mydb"
)

var count int
var mu sync.Mutex

type UrlS struct {
	id       int
	baseUrl  string
	shortUrl string
}

type DBClient struct {
	db *sql.DB
}

func newUrl(baseUrl string, id int) *UrlS {
	lUrl := UrlS{baseUrl: baseUrl, id: id}
	return &lUrl
}

type Record struct {
	ID  int    `json:"id"`
	URL string `json:"url"`
}

func encodeUrl(Url *UrlS) **UrlS {
	urlVal := int64(Url.id)
	encodedURL := base62.Encode(urlVal)
	urlLocal := "http://localhost:8000/"
	newUrl := urlLocal + encodedURL
	Url.shortUrl = newUrl
	return &Url

}

func decoderUrl(urlEncode string) int {
	byteURL, _ := base62.Decode(urlEncode)
	nVal := strconv.FormatInt(byteURL, 10)
	nValInt, _ := strconv.Atoi(nVal)
	return (nValInt)
}

func InitDB() (*sql.DB, error) {
	var connectionString = fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	var err error
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}
	stmt, err := db.Prepare("CREATE TABLE IF NOT EXISTS web_url(ID SERIAL PRIMARY KEY, URL TEXT NOT NULL);")
	if err != nil {
		return nil, err
	}
	_, err = stmt.Exec()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (driver *DBClient) newUrlHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
	mu.Lock()
	count++
	mu.Unlock()
	log.Println("test %s", vars["url"])
	newUrl(vars["url"], count)
	s := newUrl(vars["url"], count)
	encodeUrl(s)
	url := Record{}
	err := driver.db.QueryRow("INSERT INTO web_url values(  $1 , $2 );", s.id, s.shortUrl).Scan(&url)
	if err != nil {
		log.Println(err)
	}
}

func (driver *DBClient) urlHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
	idUrl := decoderUrl(vars["url"])
	url := Record{}
	err := driver.db.QueryRow("SELECT URL from web_url WHERE ID = $1", idUrl).Scan(&url)
	if err != nil {
		log.Println(err)
	}
	if url.URL != "" {
		http.Redirect(w, r, url.URL, 301)
	}
}

func main() {
	//Initialisation de la base de donnée
	db, err := InitDB()
	if err != nil {
		log.Println(err)
	}
	log.Println("Database tables are successfully initialized.")
	dbclient := &DBClient{db: db}
	r := mux.NewRouter()
	r.HandleFunc("/v1/{url}", dbclient.newUrlHandler)
	r.HandleFunc("/shorturl/{url}", dbclient.urlHandler)
	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
